# Courier Calculator

**Courier Calculator** es una calculadora de precios para los couriers disponibles en Uruguay para traer compras desde el exterior.

![Screenshot](screenshots/couriercalc01.png)

## Couriers incluidos:

* [Punto Mio](https://puntomio.uy/)
* [Gripper](https://www.gripper.com.uy/)
* [Casilla Mia](https://casillamia.uy/)
* [Envía Mi Compra](https://www.enviamicompra.com.uy/)
* [Miami Box](https://www.miami-box.com/)
* [Urubox](https://urubox.com.uy/)
* [Tienda Mía](https://tiendamia.com/uy)
* [Starbox](https://starboxuruguay.com/)
