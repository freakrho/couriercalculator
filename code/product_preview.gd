extends Panel

signal end_drag

export(NodePath) var product_name_path

var reference

func setup(data, node):
    var product_name: Label = get_node(product_name_path)
    product_name.text = data[Constants.NAME]
    reference = node
    rect_size = reference.rect_size

func _process(_delta):
    rect_size = reference.rect_size

func _enter_tree():
    GlobalSignal.add_emmiter("end_drag", "end_drag", self)
    
func _exit_tree():
    emit_signal("end_drag")
    GlobalSignal.remove_emmiter("end_drag", "end_drag", self)
