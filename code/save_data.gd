extends Node

const LAST_FILEPATH = "last_filepath"

var _data = {}
var _loaded = false

func _ready():
    load_data()

func get_last_filepath() -> String:
    _load_if_not_loaded()
    return _data[LAST_FILEPATH]

func set_last_filepath(filepath: String):
    _data[LAST_FILEPATH] = filepath
    save_data()

func save_data():
    var file = File.new();
    file.open("user://%s" % Data.save_file, File.WRITE)
    file.store_string(JSON.print(_data, "    "))
    file.close()

func _load_if_not_loaded():
    if not _loaded:
        load_data()

func  _set_default():
    _data[LAST_FILEPATH] = ""

func load_data():
    var file = File.new();
    var err = file.open("user://%s" % Data.save_file, File.READ)
    if err != OK:
        _set_default()
        return
    var result = JSON.parse(file.get_as_text())
    if result.error == OK:
        _data = result.result
    else:
        _set_default()
    file.close()
    _loaded = true
