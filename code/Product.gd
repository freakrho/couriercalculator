extends PanelContainer

signal updated_data
signal save_all
signal begin_drag

# Data
export(NodePath) var data_root_path
onready var data_root = get_node(data_root_path)
export(NodePath) var product_name_path
onready var product_name = get_node(product_name_path)
export(NodePath) var url_button_path
onready var url_button: Button = get_node(url_button_path)
export(NodePath) var product_price_path
onready var product_price = get_node(product_price_path)
export(NodePath) var product_weight_path
onready var product_weight = get_node(product_weight_path)
export(NodePath) var product_amount_path
onready var product_amount = get_node(product_amount_path)
# Edit
export(NodePath) var edit_root_path
onready var edit_root = get_node(edit_root_path)
export(NodePath) var product_name_edit_path
onready var product_name_edit = get_node(product_name_edit_path)
export(NodePath) var url_edit_path
onready var url_edit: LineEdit = get_node(url_edit_path)
export(NodePath) var product_price_edit_path
onready var product_price_edit = get_node(product_price_edit_path)
export(NodePath) var product_weight_edit_path
onready var product_weight_edit = get_node(product_weight_edit_path)
export(NodePath) var product_amount_edit_path
onready var product_amount_edit = get_node(product_amount_edit_path)

export(NodePath) var done_button_path
onready var done_button: Button = get_node(done_button_path)
export(NodePath) var edit_button_path
onready var edit_button: Button = get_node(edit_button_path)
export(PackedScene) var preview_scene

var data

func _enter_tree():
    GlobalSignal.add_emmiter("updated_data", "updated_data", self)
    GlobalSignal.add_emmiter("save_all", "save_all", self)
    GlobalSignal.add_emmiter("begin_drag", "begin_drag", self)
    
func _exit_tree():
    GlobalSignal.remove_emmiter("save_all", "save_all", self)
    GlobalSignal.remove_emmiter("updated_data", "updated_data", self)
    GlobalSignal.remove_emmiter("begin_drag", "begin_drag", self)

func setup(product):
    data = product

    data_root.visible = true;
    edit_root.visible = false;
    done_button.visible = false;
    
    product_name.text = product[Constants.NAME]
    product_amount.text = String(product[Constants.AMOUNT])
    product_price.text = "$" + String(product[Constants.PRICE])
    product_weight.text = String(product[Constants.WEIGHT]) + "kg"
    url_button.visible = Constants.URL in data and data[Constants.URL] != ""

func _on_ButtonEdit_pressed():
    if save_changes():
        emit_signal("updated_data")
    else:
        emit_signal("save_all")
        data_root.visible = false;
        edit_root.visible = true;
        done_button.visible = true;
        edit_button.visible = false;
        product_name_edit.text = data[Constants.NAME]
        product_amount_edit.text = String(data[Constants.AMOUNT])
        product_price_edit.text = String(data[Constants.PRICE])
        product_weight_edit.text = String(data[Constants.WEIGHT])
        if Constants.URL in data:
            url_edit.text = data[Constants.URL]
        else:
            url_edit.text = ""

func save_changes():
    if !data_root.visible:
        data_root.visible = true;
        edit_root.visible = false;
        done_button.visible = false;
        edit_button.visible = true;
        
        data[Constants.NAME] = product_name_edit.text
        data[Constants.AMOUNT] = int(product_amount_edit.text)
        data[Constants.PRICE] = float(product_price_edit.text)
        data[Constants.WEIGHT] = float(product_weight_edit.text)
        data[Constants.URL] = url_edit.text.strip_edges()
        
        url_button.visible = data[Constants.URL] != ""

        setup(data)
        return true
    return false

func edit_mode():
    if data_root.visible:
        _on_ButtonEdit_pressed()


func _on_ButtonDelete_pressed():
    queue_free()
    emit_signal("updated_data")

func get_drag_data(_position):
    var drag_preview = preview_scene.instance()
    drag_preview.setup(data, self)
    set_drag_preview(drag_preview)
    emit_signal("begin_drag")
    return self


func _on_ButtonURL_pressed():
    OS.shell_open(data[Constants.URL])
