extends Node

static func pow(value: float, power: int) -> float:
    if power == 0:
        return 1.0
    for _i in range(power):
        value *= value
    return value

static func ceil(value: float, dec: int = 0) -> float:
    return stepify(ceil(value * pow(10.0, dec)) / (10.0 * dec), 1.0 / pow(10.0, dec))
