extends Node

var _emmiters = {}
var _listeners = {}

func add_emmiter(signal_name: String, object_signal: String, object: Object):
    var data = {
        "object": object,
        "signal": object_signal,
    }

    if not _emmiters.has(signal_name):
        _emmiters[signal_name] = {}

    _emmiters[signal_name][object.get_instance_id()] = data
    _connect_emmiter_to_listeners(signal_name, object_signal, object)

func remove_emmiter(signal_name: String, object_signal: String, object: Object):
    if signal_name in _emmiters:
        _emmiters[signal_name].erase(object.get_instance_id())
    _disconnect_emmiter_to_listeners(signal_name, object_signal, object);

func _connect_emmiter_to_listeners(signal_name: String, object_signal: String, object: Object):
    if signal_name in _listeners:
        for key in _listeners[signal_name].keys():
            var listener = _listeners[signal_name][key]
            object.connect(object_signal, listener["object"], listener["method"])

func _disconnect_emmiter_to_listeners(signal_name: String, object_signal: String, object: Object):
    if signal_name in _listeners:
        for key in _listeners[signal_name].keys():
            var listener = _listeners[signal_name][key]
            object.disconnect(object_signal, listener["object"], listener["method"])

func add_listener(signal_name: String, object: Object, method: String):
    var data = {
        "object": object,
        "method": method,
    }

    if not _listeners.has(signal_name):
        _listeners[signal_name] = {}
    
    _listeners[signal_name][object.get_instance_id()] = data
    _connect_listener_to_emmiters(signal_name, object, method)

func remove_listener(signal_name: String, object: Object, method: String):
    if signal_name in _listeners:
        _listeners[signal_name].erase(object.get_instance_id())
    _disconnect_listener_to_emmiters(signal_name, object, method)

func _connect_listener_to_emmiters(signal_name: String, object: Object, method: String):
    if signal_name in _emmiters:
        for key in _emmiters[signal_name].keys():
            var emmiter = _emmiters[signal_name][key]
            emmiter["object"].connect(emmiter["signal"], object, method)

func _disconnect_listener_to_emmiters(signal_name: String, object: Object, method: String):
    if signal_name in _emmiters:
        for key in _emmiters[signal_name].keys():
            var emmiter = _emmiters[signal_name][key]
            emmiter["object"].disconnect(emmiter["signal"], object, method)