const WEIGHTS  = [ 0.9  ]
const PRICES  = [   19.8 ]
const WEIGHTS_PER_KG  = [ 5.0,     20.0,   40.0 ]
const PRICES_PER_KG  = [   21.90,   16.5,  13.2  ]
const HANDLING = 5.0
const MAX_PACKAGES = 5
const HANDLING_PER_PACKAGE = 1.0

static func price_per_weight(weight):
    for i in range(WEIGHTS.size()):
        if weight < WEIGHTS[i]:
            return PRICES[i]
    for i in range(WEIGHTS_PER_KG.size()):
        if weight < WEIGHTS_PER_KG[i]:
            return PRICES_PER_KG[i] * weight

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var packages = purchase[Constants.PACKAGES]
    
    var total = HANDLING;
    if packages > MAX_PACKAGES:
        total += (MAX_PACKAGES - packages) * HANDLING_PER_PACKAGE;
    
    total += price_per_weight(weight)

    return {
        Constants.PRICE: total,
        Constants.ESTIMATED_WITH_TAX: total * (1 + Constants.TFSPU),
    }

static func get_calculator():
    return "https://www.gripper.com.uy/calculadora"
        
static func get_name():
    return "Gripper"