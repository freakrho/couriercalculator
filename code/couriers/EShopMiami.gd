const PRICE_PER_KG = 22.9
const HANDLING = 6.0
const DELIVERY_COST = 2.0
const IVA = 0.22
const SHIPPING_IVA = 0.03

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var shipping = weight * PRICE_PER_KG
    var tfspu = (shipping + HANDLING) * Constants.TFSPU
    var iva = (shipping + HANDLING) * SHIPPING_IVA * IVA + DELIVERY_COST * IVA
    var result = shipping + HANDLING + DELIVERY_COST + tfspu + iva

    return {
        Constants.PRICE: result,
        Constants.ESTIMATED_WITH_TAX: result,
    }

static func get_calculator():
    return "https://www.eshopmiami.com.uy/es/simplepages/calculator"

static func get_name():
    return "eShopMiami"
