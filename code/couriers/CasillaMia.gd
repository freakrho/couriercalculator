const WEIGHTS = [
    5.0,    
    20.0,
    999.0,
]
const PRICE_PER_500G = [
    10.0,
    4.0,
    6.0,
]
const WEIGHT_STEP = 0.5
const COST_PER_PACKAGE = 2.0


static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var packages = purchase[Constants.PACKAGES]
    
    var price = packages * COST_PER_PACKAGE
    for i in range(PRICE_PER_500G.size()):
        var tier = WEIGHTS[i]
        price += ceil(min(tier, weight) / WEIGHT_STEP) * PRICE_PER_500G[i]
        if weight <= tier:
            break
    
    price *= (1 + Constants.TFSPU)

    return {
        Constants.PRICE: price,
        Constants.ESTIMATED_WITH_TAX: price,
    }

static func get_calculator():
    return "https://casillamia.uy/calcrates"

static func get_name():
    return "Casilla Mia"