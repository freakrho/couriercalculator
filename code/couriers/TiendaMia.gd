const PRICE_PER_KG = 21.99
const DISCOUNT_BETWEEN_3_1_AND_5 = 0.3
const TAX = 0.095
const HANDLING_PER_PRODUCT = 2.99
const MAX_PRODUCTS_PER_HANDLING = 3
const DELIVERY_COST = 4.99

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var price = purchase[Constants.PRICE]
    var amount = purchase[Constants.AMOUNT]
    
    # flat rate
    var shipping_price = DELIVERY_COST
    
    # weight
    var discount = 0.0
    if weight > 3.0:
        discount = DISCOUNT_BETWEEN_3_1_AND_5
    
    shipping_price += PRICE_PER_KG * (1.0 - discount) * weight

    # tax
    shipping_price += price * TAX
    
    # handling
    shipping_price += min(MAX_PRODUCTS_PER_HANDLING, amount) * HANDLING_PER_PRODUCT

    
    return {
        Constants.PRICE: shipping_price,
        Constants.ESTIMATED_WITH_TAX: shipping_price * (1 + Constants.TFSPU),
    }

static func get_calculator():
    return "https://tiendamia.com/tarifas"
    
static func get_name():
    return "Tienda Mía"