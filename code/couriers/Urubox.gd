const WEIGHTS = [ 0.2,     0.5,   0.7,    1.0]
const PRICES = [   10.90,   15.90, 18.90,  20.90]
const WEIGHTS_PER_KG = [ 5.0,     10.0,   20.0,   40.0]
const PRICES_PER_KG = [   17.90,   17.90,  16.50,  15.90]
const DELIVERY_COST = 6.15  # $5 + IVA
const HANDLING = 5.0

static func price_per_weight(weight):
    for i in range(WEIGHTS.size()):
        if weight < WEIGHTS[i]:
            return PRICES[i]
    for i in range(WEIGHTS_PER_KG.size()):
        if weight < WEIGHTS_PER_KG[i]:
            return PRICES_PER_KG[i] * weight

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var total = DELIVERY_COST + HANDLING

    total = apply_tfspu(total + price_per_weight(weight))

    return {
        Constants.PRICE: total,
        Constants.ESTIMATED_WITH_TAX: total,
    }


static func apply_tfspu(value):
    return value * (1 + Constants.TFSPU)

static func get_calculator():
    return "https://urubox.com.uy/calculadora.html"
    
static func get_name():
    return "Urubox"