const WEIGHTS = [
    0.5,
    0.6,
    5.0,
    10.0,
    20.0,
]
const PRICE_PER_KG = [
    11.99,
    15.5,
    23.5,
    20.5,
    17.5,
]
const HANDLING = 5

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    
    var price = HANDLING
    for i in range(PRICE_PER_KG.size()):
        var tier = WEIGHTS[i]
        if weight <= tier:
            price += PRICE_PER_KG[i] * Math.ceil(weight, 1)
            break
    
    price *= (1 + Constants.TFSPU)
    price += HANDLING * Constants.IVA

    return {
        Constants.PRICE: price,
        Constants.ESTIMATED_WITH_TAX: price,
    }

static func get_calculator():
    return "https://aerobox.com.uy/calculadora-envios/"

static func get_name():
    return "Aerobox"