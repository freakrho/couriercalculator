const VALUE = 17.5
const CONSTANT = 0.041  # no clue what this is

static func price(purchase):
    var weight = purchase[Constants.WEIGHT] + CONSTANT
    weight = stepify(weight, 0.1)  # round to 100g
    var value = weight * VALUE

    return {
        Constants.PRICE: value,
        Constants.ESTIMATED_WITH_TAX: value * (1 + Constants.TFSPU),
    }


static func get_calculator():
    return "https://www.usxcargo.com/index.php"

static func get_name():
    return "USX"
        