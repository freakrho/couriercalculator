const BASE_COST = 18
const COST_PER_POUND = 7.50

static func price(purchase):
    var total = BASE_COST
    var weight = purchase[Constants.WEIGHT]
    var pounds = weight * Constants.KILOGRAMS_TO_POUNDS
    if pounds > 1:
        total += COST_PER_POUND * (stepify(ceil(pounds * 10.0) / 10.0, 0.1) - 1)
    
    return {
        Constants.PRICE: total,
        Constants.ESTIMATED_WITH_TAX: total * (1 + Constants.TFSPU)
    }

static func get_calculator():
    return "https://www.exurenvios.com/Calculadora.aspx"

static func get_name():
    return "EXUR"
        