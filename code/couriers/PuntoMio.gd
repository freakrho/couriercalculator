
const WEIGHTS = [
    0.5,
    1.0,
]

const PRICES = [
    8.99,
    13.0,
]

const WEIGHTS_PER_KG = [
    1.5,
    2.0,
    2.5,
    3.0,
    3.5,
    4.0,
    4.5,
]

const PRICES_PER_KG = [
    27.0,
    34.0,
    41.0,
    48.0,
    55.0,
    62.0,
    69.0,
]

const HANDLING = 5.0
const MAX_PACKAGES = 4
const HANDLING_PER_PACKAGE = 2.0

static func price_per_weight(weight):
    for i in range(WEIGHTS.size()):
        if weight <= WEIGHTS[i]:
            return PRICES[i]
    
    for i in range(WEIGHTS_PER_KG.size()):
        if weight <= WEIGHTS_PER_KG[i]:
            return PRICES_PER_KG[i] * weight

static func price(_purchase):
    # var weight = purchase[Constants.WEIGHT]
    # var packages = purchase[Constants.PACKAGES]
    
    # var total = HANDLING
    # if packages > MAX_PACKAGES:
    #     total += (MAX_PACKAGES - packages) * HANDLING_PER_PACKAGE
    
    # total += price_per_weight(weight)
    var total = 99999

    return {
        Constants.PRICE: total,
        Constants.ESTIMATED_WITH_TAX: total,
    }

static func get_calculator():
    return "https://puntomio.uy/calculadora"
        
static func get_name():
    return "PuntoMio**"