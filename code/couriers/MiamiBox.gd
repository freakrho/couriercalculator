const WEIGHTS = [
    9.9,
    19.9,
    29.9,
]
const FRACTIONS = [
    1.0,
    0.8,
    0.7,
]
const PRICE_PER_100G = 2.5
const UNDER_400_MIN_PRICE= 10.0
const HANDLING = 6.0

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var price = HANDLING;
    if weight < 0.4:
        price += UNDER_400_MIN_PRICE;
    
    for i in range(WEIGHTS.size()):
        if weight <= WEIGHTS[i]:
            price += weight / 0.1 * PRICE_PER_100G * FRACTIONS[i]
            break

    return {
        Constants.PRICE: price,
        Constants.ESTIMATED_WITH_TAX: price * (1 + Constants.TFSPU),
    }

static func get_calculator():
    return "https://www.miami-box.com/calculadora"

static func get_name():
    return "MiamiBox"