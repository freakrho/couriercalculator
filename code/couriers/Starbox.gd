const WEIGHTS = [
    0.5,
    0.999,
]
const PRICES = [
    17.0,
    21.0
]
const WEIGHTS_PER_KG = [
    4.99,
    10.0,
]
const PRICES_PER_KG = [
    21.0,
    20.0,
]
const HANDLING_AND_SHIPPING = 5
const DELIVERY_MVD = 10.0

static func price_per_weight(weight):
    for i in range(WEIGHTS.size()):
        if weight < WEIGHTS[i]:
            return PRICES[i]
    for i in range(WEIGHTS_PER_KG.size()):
        if weight < WEIGHTS_PER_KG[i]:
            return PRICES_PER_KG[i] * ceil(weight / 0.1) * 0.1
    return 0.0

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    var total = HANDLING_AND_SHIPPING + price_per_weight(weight)
    # Taxes
    total *= 1 + Constants.TFSPU
    total += (HANDLING_AND_SHIPPING + DELIVERY_MVD) * Constants.IVA
    # Delivery MVD
    total += DELIVERY_MVD * 1.23
    
    return {
        Constants.PRICE: total,
        Constants.ESTIMATED_WITH_TAX: total,
    }

static func get_calculator():
    return "https://starboxuruguay.com/calculadora"
    
static func get_name():
    return "Starbox"
