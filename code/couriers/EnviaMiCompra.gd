const WEIGHTS = [
    0.0,
    5.0,
    10.0,
    20.0,
]
const PRICES = [
    21.9,
    20.9,
    16.5,
    11.9,
]

const HANDLING = 5.0

static func price(purchase):
    var weight = purchase[Constants.WEIGHT]
    
    var value = 0
    for i in range(WEIGHTS.size(), 0, -1):
        if weight > WEIGHTS[i-1]:
            value = weight * PRICES[i-1]
            break

    var price = (value + HANDLING) * (1 + Constants.TFSPU) + value * Constants.IVA_SHIPPING

    return {
        Constants.PRICE: price,
        Constants.ESTIMATED_WITH_TAX: price,
    }

static func get_calculator():
    return "https://www.enviamicompra.com.uy/calculator"
        
static func get_name():
    return "Envía Mi Compra"