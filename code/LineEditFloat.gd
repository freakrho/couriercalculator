extends LineEdit

func _ready():
    connect("text_entered", self, "_text_entered")

func _text_entered(new_text: String):
    text = String(float(new_text))
