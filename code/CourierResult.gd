extends Button

export(NodePath) var result_label_path
onready var result_label: Label = get_node(result_label_path)
export(NodePath) var name_path
onready var name_label: Label = get_node(name_path)

var value
var courier

func _ready():
    connect("pressed", self, "_on_press")

func setup(a_value):
    value = stepify(a_value[Constants.ESTIMATED_WITH_TAX], 0.01)
    var price = stepify(a_value[Constants.PRICE], 0.01)
    var result_txt = String(price)
    if value != price:
        result_txt += "/$%s*" % stepify(value, 0.01)
    result_label.text = "$%s" % result_txt

func set_index(index: int):
    get_parent().move_child(self, index)

func _on_press():
    OS.shell_open(courier.get_calculator())

func set_courier(a_courier):
    courier = a_courier
    name_label.text = courier.get_name()
