extends Node

# Keys
const WEIGHT = "weight"
const PACKAGES = "packages"
const PRODUCTS = "products"
const AMOUNT = "amount"
const PRICE = "price"
const SHIPPING = "shipping"
const ESTIMATED_WITH_TAX = "estimated_with_tax"
const NAME = "name"
const URL = "url"

# Values
const TFSPU = 0.1
const IVA_SHIPPING = 0.0066
const IVA = 0.23
const KILOGRAMS_TO_POUNDS = 2.204623