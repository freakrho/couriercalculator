extends Control

const Product = preload("res://code/Product.gd")

signal updated_data

var package

func _enter_tree():
    GlobalSignal.add_emmiter("updated_data", "updated_data", self)

func _exit_tree():
    GlobalSignal.remove_emmiter("updated_data", "updated_data", self)

func can_drop_data(_position, data):
    return data is Product

func drop_data(_position, data):
    data.get_parent().remove_child(data)
    package.products_root.add_child(data)
    emit_signal("updated_data")
