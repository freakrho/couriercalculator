extends Control

export(NodePath) var file_dialog_path
onready var file_dialog = get_node(file_dialog_path)
export(NodePath) var save_file_dialog_path
onready var save_file_dialog = get_node(save_file_dialog_path)
export(NodePath) var purchase_root_path
onready var purchase_root = get_node(purchase_root_path)
export(PackedScene) var packageScene

# Results
export(NodePath) var total_amount_path
onready var total_amount_label: Label = get_node(total_amount_path)
export(NodePath) var total_packages_path
onready var total_packages_label: Label = get_node(total_packages_path)
export(NodePath) var total_weight_path
onready var total_weight_label: Label = get_node(total_weight_path)
export(NodePath) var total_price_path
onready var total_price_label: Label = get_node(total_price_path)

# Couriers
export(NodePath) var result_root_path
onready var result_root: Node = get_node(result_root_path)
export(PackedScene) var result_scene: PackedScene

# Undo/Redo
export(NodePath) var undo_button_path
onready var undo_button: Button = get_node(undo_button_path)
export(NodePath) var redo_button_path
onready var redo_button: Button = get_node(redo_button_path)

const CasillaMia = preload("res://code/couriers/CasillaMia.gd")
const Gripper = preload("res://code/couriers/Gripper.gd")
const MiamiBox = preload("res://code/couriers/MiamiBox.gd")
const PuntoMio = preload("res://code/couriers/PuntoMio.gd")
const Starbox = preload("res://code/couriers/Starbox.gd")
const TiendaMia = preload("res://code/couriers/TiendaMia.gd")
const Urubox = preload("res://code/couriers/Urubox.gd")
const EnviaMiCompra = preload("res://code/couriers/EnviaMiCompra.gd")
const USX = preload("res://code/couriers/USX.gd")
const Exur = preload("res://code/couriers/Exur.gd")
const Aerobox = preload("res://code/couriers/Aerobox.gd")
const EShopMiami = preload("res://code/couriers/EShopMiami.gd")


var packages: Array
var steps: Array = []
var current_step = 0
var results: Dictionary

class Result:
    var result

    func _init(a_result):
        result = a_result

    static func sort_ascending(a, b):
        return a.result.value < b.result.value
    
    func setup(value):
        result.setup(value)


func _ready():
    clear_packages()
    clear_steps()
    results = {
        CasillaMia: Result.new(create_result_data(CasillaMia)),
        MiamiBox: Result.new(create_result_data(MiamiBox)),
        PuntoMio: Result.new(create_result_data(PuntoMio)),
        Starbox: Result.new(create_result_data(Starbox)),
        TiendaMia: Result.new(create_result_data(TiendaMia)),
        Urubox: Result.new(create_result_data(Urubox)),
        Gripper: Result.new(create_result_data(Gripper)),
        EnviaMiCompra: Result.new(create_result_data(EnviaMiCompra)),
        USX: Result.new(create_result_data(USX)),
        Exur: Result.new(create_result_data(Exur)),
        Aerobox: Result.new(create_result_data(Aerobox)),
        EShopMiami: Result.new(create_result_data(EShopMiami)),
    }
    
    load_data_from_file(SaveData.get_last_filepath())

func _enter_tree():
    GlobalSignal.add_listener("updated_data", self, "update_prices")
    GlobalSignal.add_listener("save_all", self, "save_all")

func _exit_tree():
    GlobalSignal.remove_listener("updated_data", self, "update_prices")
    GlobalSignal.remove_listener("save_all", self, "save_all")

func set_window_title(filepath: String):
    OS.set_window_title("%s - %s" % [ProjectSettings.get_setting("application/config/name"), filepath.get_file()])

func load_purchase(filepath):
    var file = File.new()
    var err = file.open(filepath, File.READ)
    if err == OK:
        SaveData.set_last_filepath(filepath)
        set_window_title(filepath)

        var purchase = JSON.parse(file.get_as_text())
        if purchase.error:
            print(purchase.error_string)
            return
        return purchase.result
    return { Constants.PACKAGES: [] }

func get_purchase_data(purchase):
    var weight = 0.0
    var price = 0.0
    var amount = 0
    var package_amount = 0
    for package in purchase[Constants.PACKAGES]:
        package_amount += 1
        if Constants.SHIPPING in package:
            price += package[Constants.SHIPPING]
        for product in package[Constants.PRODUCTS]:
            var a = product[Constants.AMOUNT]
            amount += a
            weight += a * product[Constants.WEIGHT]
            price += a * product[Constants.PRICE]
    
    return {
        Constants.WEIGHT: weight,
        Constants.PRICE: price,
        Constants.AMOUNT: amount,
        Constants.PACKAGES: package_amount,
    }


func _on_LoadButton_pressed():
    file_dialog.popup_centered()

func clear_packages():
    packages = []
    for node in purchase_root.get_children():
        purchase_root.remove_child(node)

func load_data_from_file(path: String):
    var purchase = load_purchase(path)
    load_purchase_data(purchase)
    update_prices()

func _on_FileDialog_file_selected(path: String):
    clear_steps()
    load_data_from_file(path)
    
func load_purchase_data(purchase):
    clear_packages()
    for package in purchase[Constants.PACKAGES]:
        var pkg = packageScene.instance()
        purchase_root.add_child(pkg)
        pkg.populate(package)
        packages.append(pkg)

func _on_ButtonSave_pressed():
    var last_file = SaveData.get_last_filepath()
    if last_file == "":
        _on_ButtonSaveNew_pressed()
    else:
        var file = File.new()
        file.open(last_file, File.WRITE)
        file.store_string(JSON.print(get_purchase(), "  "))
    
func get_purchase():
    var package_list = []
    for pkg in packages:
        if !is_instance_valid(pkg) || pkg.is_queued_for_deletion():
            continue
        package_list.append(pkg.get_package())
    
    return {
        Constants.PACKAGES: package_list,
    }
    
func create_result_data(courier):
    var result = result_scene.instance()
    result_root.add_child(result)
    result.set_courier(courier)
    return result

func update_prices(register_step=true):
    var purchase = get_purchase()
    if register_step:
        save_step(purchase)

    var total = get_purchase_data(purchase)
    print(total)
    total_amount_label.text = String(total[Constants.AMOUNT])
    total_packages_label.text = String(total[Constants.PACKAGES])
    total_weight_label.text = String(stepify(total[Constants.WEIGHT], 0.001)) + "kg"
    total_price_label.text = "$" + String(stepify(total[Constants.PRICE], 0.01))

    var prices = []
    for key in results.keys():
        var result = results[key]
        result.setup(key.price(total))
        prices.append(result)
    
    prices.sort_custom(Result, "sort_ascending")
    for i in range(prices.size()):
        var result: Result = prices[i]
        result.result.set_index(i)
    
    update_undo_redo_buttons()

func _on_ButtonAddPackage_pressed():
    save_all()
    var pkg = packageScene.instance()
    purchase_root.add_child(pkg)
    pkg.populate({
        Constants.NAME: "Nuevo paquete",
        Constants.PRODUCTS: [],
    })
    packages.append(pkg)
    pkg.edit_mode()


func _on_ButtonSaveNew_pressed():
    save_file_dialog.popup_centered()


func _on_SaveDialog_file_selected(path:String):
    SaveData.set_last_filepath(path)
    _on_ButtonSave_pressed()

func save_step(step):
    # remove steps that are ahead of current
    while steps.size() > current_step + 1:
        steps.remove(steps.size() - 1)
    
    steps.append(step)

    #remove steps until we are on max size
    while steps.size() > Data.max_steps:
        steps.remove(0)
    current_step = steps.size() - 1
    
func clear_steps():
    steps.clear()
    save_step({ Constants.PACKAGES: [] })
    update_undo_redo_buttons()
    
func undo():
    if !can_undo():
        return
    current_step -= 1
    var purchase = steps[current_step]
    load_purchase_data(purchase)
    update_prices(false)
    
func redo():
    if !can_redo():
        return
    current_step += 1
    var purchase = steps[current_step]
    load_purchase_data(purchase)
    update_prices(false)

func can_undo():
    return current_step >= 1

func can_redo():
    return current_step < steps.size() - 1

func update_undo_redo_buttons():
    undo_button.disabled = !can_undo()
    redo_button.disabled = !can_redo()

func save_all():
    for pkg in packages:
        if !is_instance_valid(pkg) || pkg.is_queued_for_deletion():
            continue
        
        pkg.save_changes()
    update_prices()
