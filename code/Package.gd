extends PanelContainer

signal updated_data
signal save_all

export(NodePath) var package_name_path
onready var package_name_label: Label = get_node(package_name_path)
export(NodePath) var package_price_path
onready var package_price_label: Label = get_node(package_price_path)
export(NodePath) var package_shipping_path
onready var package_shipping_label: Label = get_node(package_shipping_path)
export(NodePath) var package_name_edit_path
onready var package_name_edit: LineEdit = get_node(package_name_edit_path)
export(NodePath) var package_shipping_edit_path
onready var package_shipping_edit: LineEdit = get_node(package_shipping_edit_path)
export(NodePath) var products_root_path
onready var products_root: Node = get_node(products_root_path)
export(NodePath) var done_button_path
onready var done_button: Button = get_node(done_button_path)
export(NodePath) var edit_button_path
onready var edit_button: Button = get_node(edit_button_path)
export(NodePath) var drag_target_path
onready var drag_target: Control = get_node(drag_target_path)
export(PackedScene) var productScene

var package_name: String
var package

func _ready():
    drag_target.visible = false
    drag_target.package = self

func _enter_tree():
    GlobalSignal.add_emmiter("updated_data", "updated_data", self)
    GlobalSignal.add_emmiter("save_all", "save_all", self)
    GlobalSignal.add_listener("begin_drag", self, "_on_begin_drag")
    GlobalSignal.add_listener("end_drag", self, "_on_end_drag")
    GlobalSignal.add_listener("updated_data", self, "set_price")
    
func _exit_tree():
    GlobalSignal.remove_emmiter("updated_data", "updated_data", self)
    GlobalSignal.remove_emmiter("save_all", "save_all", self)
    GlobalSignal.remove_listener("begin_drag", self, "_on_begin_drag")
    GlobalSignal.remove_listener("end_drag", self, "_on_end_drag")
    GlobalSignal.remove_listener("updated_data", self, "set_price")

func get_shipping() -> float:
    if Constants.SHIPPING in package:
        return package[Constants.SHIPPING]
    return 0.0

func populate(pkg):
    package = pkg
    package_name = pkg[Constants.NAME]

    package_shipping_label.text = "$" + String(get_shipping())

    package_name_label.text = package_name
    package_name_edit.visible = false
    package_shipping_edit.visible = false
    done_button.visible = false;
    for product in pkg[Constants.PRODUCTS]:
        add_product(product)
    set_price()
        
func set_price():
    package = get_package()
    var total = get_shipping()
    for product in package[Constants.PRODUCTS]:
        total += product[Constants.PRICE] * product[Constants.AMOUNT]
    package_price_label.text = "$" + String(total)

func add_product(product):
    var p = productScene.instance()
    products_root.add_child(p)
    p.setup(product)
    emit_signal("save_all")
    return p

func _on_ButtonEdit_pressed():
    if !package_name_label.visible:
        set_package_data(package_name_edit.text, float(package_shipping_edit.text))
        emit_signal("mouse_entered")
        emit_signal("updated_data")
    else:
        emit_signal("save_all")
        package_name_label.visible = false
        package_name_edit.visible = true
        package_shipping_edit.visible = true
        package_shipping_label.visible = false
        done_button.visible = true;
        edit_button.visible = false;
        package_name_edit.text = package_name_label.text
        package_shipping_edit.text = String(get_shipping())
        
func set_package_data(new_name: String, new_shipping: float):
    package[Constants.SHIPPING] = new_shipping
    package_shipping_label.text = "$" + String(new_shipping)
    package_name = new_name
    package_name_label.text = new_name

    package_name_label.visible = true
    package_name_edit.visible = false
    package_shipping_label.visible = true
    package_shipping_edit.visible = false
    
    done_button.visible = false;
    edit_button.visible = true;

func get_package():
    var product_list = []
    for product in products_root.get_children():
        if !is_instance_valid(product) || product.is_queued_for_deletion():
            continue
        product_list.append(product.data)
    return {
        Constants.NAME: package_name,
        Constants.SHIPPING: get_shipping(),
        Constants.PRODUCTS: product_list,
    }

func edit_mode():
    _on_ButtonEdit_pressed()

func save_changes():
    if !package_name_label.visible:
        set_package_data(package_name_edit.text, float(package_shipping_edit.text))
    
    for product in products_root.get_children():
        if !is_instance_valid(product) || product.is_queued_for_deletion():
            continue
        product.save_changes()


func _on_ButtonAddProduct_pressed():
    var p = add_product({
        Constants.NAME: "Producto",
        Constants.PRICE: 0.0,
        Constants.WEIGHT: 0.0,
        Constants.AMOUNT: 1,
    })
    p.edit_mode()

func _on_ButtonRemove_pressed():
    queue_free()
    emit_signal("updated_data")

func _on_begin_drag():
    drag_target.visible = true

func _on_end_drag():
    drag_target.visible = false
